# Sparks

Sparks is a Python library for dealing with word pluralization.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash

pip install LOCAL
pip install GITLAB
```

## Usage

```python
import foobar

# returns 'words'
=======
foobar.pluralize('GITLAB')

# returns 'geese'
foobar.pluralize('GITLAB')
>>>>>>> refs/remotes/origin/master

# returns 'phenomenon'
foobar.singularize('phenomena')
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
